'use strict';

describe('Component: MemberAdd', function () {

  var component, memberResource, $q, $compile, $scope,$httpBackend;
  var mockMemberResponse = {
    name: 'test-name',
    contactList: [{
      name: 'test-contact-name',
      email: 'test-contact-email',
      phone: 'test-contact-phone'
    }]
  };

  beforeEach(function () {
    module('memberApp');
    module('templates');
    inject(function (_$componentController_, _MemberResource_, _$q_, _$compile_, _$rootScope_,_$httpBackend_) {
      $q = _$q_;
      $scope = _$rootScope_.$new();
      $compile = _$compile_;
      $httpBackend=_$httpBackend_;
      memberResource = _MemberResource_;
      $httpBackend.whenGET().respond(200, '');
      component = _$componentController_('memberAdd', {
        MemberResource: memberResource
      });
    });
  });

  it('should successfully make request to add member', function () {
    spyOn(memberResource, "save").and.callFake(function(params, successCallBack) {
      successCallBack({
        name: 'test-name',
        id: 'test-id'
      });
    });

    component.memberName = mockMemberResponse.name;
    var contactList=[];

    var contact = {
      name:  mockMemberResponse.contactList[0].name,
      email: mockMemberResponse.contactList[0].email,
      phone: mockMemberResponse.contactList[0].phone
    }
    contactList.push(contact);
    component.contactList = contactList;

    expect(component.successMsg).toBeUndefined();
    expect(component.successId).toBeUndefined();

    component.addMember(); // method under test

    expect(memberResource.save).toHaveBeenCalledWith({
      name:component.memberName,
      contactList: component.contactList
    }, jasmine.any(Function), jasmine.any(Function));

    expect(component.successMsg.indexOf('test-name')).not.toBe(-1);
    expect(component.successId).toBe("test-id");

  });

  it('should handle failed request to add member', function () {
    spyOn(memberResource, "save").and.callFake(function(params, successCallBack, failureCallBack) {
      failureCallBack({
        status: '404',
        data: 'not found'
      });
    });

    component.name = mockMemberResponse.name;
    var contactList=[];

    var contact = {
      name:  mockMemberResponse.contactList[0].name,
      email: mockMemberResponse.contactList[0].email,
      phone: mockMemberResponse.contactList[0].phone
    }
    contactList.push(contact);
    component.contactList = contactList;

    expect(component.errorMsg).toBeUndefined();

    component.addMember(); // method under test

    expect(memberResource.save).toHaveBeenCalledWith({
      name:component.memberName,
      contactList:component.contactList
    }, jasmine.any(Function), jasmine.any(Function));

    expect(component.errorMsg.indexOf('404')).not.toBe(-1);
    expect(component.errorMsg.indexOf('not found')).not.toBe(-1);

  });

  // TODO implement the test that checks that the <member-add></member-add> component when used renders the add member
  // form
  it('should render component', function() {
    var element = $compile(angular.element('<member-add></member-add>'))($scope);
    $scope.$digest();
    console.log(element);
    expect(element.find('member-name')).toBe('member-name');
    expect(element.find('contact-name')).toBe('contact-name');
    expect(element.find('contact-phone')).toBe('contact-phone');
    expect(element.find('contact-email')).toBe('contact-email');
  });
});