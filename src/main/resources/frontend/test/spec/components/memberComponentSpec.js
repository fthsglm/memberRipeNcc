'use strict';

describe('Component: Member', function () {

  var component, memberResource, routeParams, $compile, $scope, $q;
  var mockMemberResponse = {
    name: 'test-name-1',
    id: 'test-id-1',
    contactList: [{
      name: 'test-contact-name-1',
      email: 'test-contact-email-1',
      phone: 'test-contact-phone-1'
    },{
      name: 'test-contact-name-2',
      email: 'test-contact-email-2',
      phone: 'test-contact-phone-2'
    }]};

  beforeEach(function () {
    module('memberApp');
    module('templates');
    inject(function (_$componentController_, _MemberResource_, _$routeParams_, _$compile_, _$rootScope_,_$q_) {
      $q = _$q_;
      $scope = _$rootScope_.$new();
      $compile = _$compile_;
      memberResource = _MemberResource_;
      routeParams = _$routeParams_;
      component = _$componentController_('member', {
        MemberResource: memberResource,
        $routeParams: routeParams
      });
    });
  });

  it('should make request to get member on load', function () {
    var testMemberShipId = 123;
    routeParams.id = testMemberShipId;
    var deferred = $q.defer();
    deferred.resolve(mockMemberResponse);
    spyOn(memberResource, "getMember").and.returnValue({$promise : deferred.promise});
    expect(component.tableParams).toBeUndefined();
    component.$onInit();
    expect(memberResource.getMember).toHaveBeenCalledWith({id:testMemberShipId});
  });

  it('should render component', function() {
    var testMemberShipId = 123;
    routeParams.id = testMemberShipId;
    var deferred = $q.defer();
    deferred.resolve(mockMemberResponse);
    spyOn(memberResource, "getMember").and.returnValue({$promise : deferred.promise});
    expect(component.tableParams).toBeUndefined();
    var element = $compile(angular.element("<member></member>"))($scope);
    $scope.$digest();

    expect(element.find('td[data-title-text=\'Name\']')[0].innerHTML).toBe('test-contact-name-1');
    expect(element.find('td[data-title-text=\'Email\']')[0].innerHTML).toBe('test-contact-email-1');
    expect(element.find('td[data-title-text=\'Phone\']')[0].innerHTML).toBe('test-contact-phone-1');
    expect(element.find('td[data-title-text=\'Name\']')[1].innerHTML).toBe('test-contact-name-2');
    expect(element.find('td[data-title-text=\'Email\']')[1].innerHTML).toBe('test-contact-email-2');
    expect(element.find('td[data-title-text=\'Phone\']')[1].innerHTML).toBe('test-contact-phone-2');
  });
});