
angular.module('memberApp').component('memberAdd', {
  templateUrl: 'component/member-add/member-add-view.html',
  controller: memberAddController
});

memberAddController.$inject = ['MemberResource'];


function memberAddController(MemberResource) {
  var vm = this;

  

  vm.isMemberNameReadOnly=function(){
    return vm.contactList.length>0;
  }

  vm.addMemberVisible=function(){
    return vm.memberName!=null&&((vm.contactPhone!=null&&vm.contactEmail!=null&&vm.contactName!=null)||(vm.contactList.length>0))
  }

  vm.addContactVisible=function(){
    return vm.contactPhone!=null&&vm.contactEmail!=null&&vm.contactName!=null;
  }

  vm.addMember = function() {
    this.addContact();
    var onSuccess = function(successResponse) {
      vm.successMsg = successResponse.name + ' successfully added.';
      vm.successId = successResponse.id;
    };

    var onFailure = function (errorResponse) {
      vm.errorMsg = errorResponse.status + '(' + errorResponse.data + ')';
    };

    MemberResource.save({
      name: vm.memberName,
      contactList: vm.contactList
    }, onSuccess, onFailure);
  }

  vm.addContact = function() {
    vm.contactList.push({
      name: vm.contactName,
      email: vm.contactEmail,
      phone: vm.contactPhone
    });
    
     vm.contactName=null;
     vm.contactEmail=null;
     vm.contactPhone=null;
  }

  vm.$onInit = function () {
    vm.contactList = [];
  }
}