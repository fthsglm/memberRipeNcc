
angular.module('memberApp').component('member', {
  templateUrl: 'component/member/member-view.html',
  controller: memberController
});


memberController.$inject = ['MemberResource', '$routeParams', 'NgTableParams'];
function memberController(MemberResource, $routeParams,NgTableParams) {
  var vm = this;

  vm.$onInit = function () {
    
    MemberResource.getMember({id:$routeParams.id}).$promise.then(function(response){
      vm.member=response;
      vm.tableParams = new NgTableParams({}, {
        dataset: vm.member.contactList,
        counts: []
      });
    });
 
  }

}