package ripe.ncc.member;

public class Constants {

    public static final String EMAIL_VALIDATOR = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";
    public static final String NAME_VALIDATOR = "[a-zA-Z]+\\.?";
    public static final String PHONE_VALIDATOR = "\\d{10}$";
}
