package ripe.ncc.member.handler;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ripe.ncc.member.exception.ErrorMessage;
import ripe.ncc.member.exception.MemberException;
import ripe.ncc.member.model.Member;
import ripe.ncc.member.service.MemberService;

import java.util.List;

@RestController
@RequestMapping("/members")
@Slf4j
public class MemberHandler {

    @Autowired
    private MemberService memberService;

    /**
     * This method gets all members from service
     * returns an object which is {@link ResponseEntity<>} class format.
     */
    @GetMapping
    public ResponseEntity getAllMembers() {
        List<Member> memberList = memberService.findAll();
        log.warn("Get all member method hit");
        return new ResponseEntity(memberList, HttpStatus.OK);
    }

    /**
     * This method resolve id from url as a parameter ,and used this id as a argument for itself.
     * returns an object which is {@link ResponseEntity<>} class format.
     * If this method works fine, it returns  HttpStatus.OK, if not HttpStatus.NOT_FOUND
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity getMemberById(@PathVariable("id") Long id) {
        try {
            log.info("Get member by id = " + id);
            Member member = memberService.findById(id);
            return new ResponseEntity(member, HttpStatus.OK);
        } catch (MemberException me) {
            log.error(me.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(me.getMessage());
        }
    }


    /**
     * This method saves a member which is located in RequestBody in JSON type,
     * and returns an object which is {@link ResponseEntity<>} class format.
     * If this method works fine, it returns  HttpStatus.CREATED, if not HttpStatus.BAD_REQUEST
     */
    @PostMapping
    public ResponseEntity addMember(@RequestBody Member member) {

        try {
            log.info("Member is been adding with name = " + member.getMemberName());
            Member savedMember = memberService.addMember(member);
            return new ResponseEntity(savedMember, HttpStatus.CREATED);
        } catch (MemberException me) {
            log.error(me.getMessage());
            return new ResponseEntity(me.getBody(),HttpStatus.BAD_REQUEST);
        }
    }
}
