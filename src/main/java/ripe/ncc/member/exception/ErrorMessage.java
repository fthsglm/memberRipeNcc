package ripe.ncc.member.exception;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class ErrorMessage implements Serializable {

    private String statusText;
    private String message;

    public ErrorMessage(String statusText, String message) {
        this.statusText = statusText;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getStatusText() {
        return statusText;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "statusText='" + statusText + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
