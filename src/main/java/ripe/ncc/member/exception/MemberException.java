package ripe.ncc.member.exception;

import org.springframework.http.HttpStatus;

public class MemberException extends RuntimeException {

    private String message;

    public MemberException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MemberException{" +
                "message='" + message + '\'' +
                '}';
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"").append(getMessage()).append("\"");
        return sb.toString();
    }
}
