package ripe.ncc.member.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import ripe.ncc.member.Constants;
import ripe.ncc.member.exception.MemberException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static ripe.ncc.member.util.StringValidatorUtil.validateNullFalse;

@Entity(name = "MEMBER")
public class Member implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    @JsonProperty("name")
    private String memberName;

    @OneToMany(targetEntity=Contact.class,cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true)
    @JsonProperty("contactList")
    private Collection<Contact> contacts;

    protected static final String INVALID_MEMBER_NAME = "Invalid member name";
    protected static final String CONTACT_IS_EMPTY = "Contact is empty";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Collection<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Collection<Contact> contacts) {
        this.contacts = contacts;
    }

    /**
     * This method throws a MemberException if memberName is null or contacts is empty or null
*/
    public void validate() throws MemberException{
        if (!validateNullFalse(getMemberName(), Constants.NAME_VALIDATOR)) {
            throw new MemberException(INVALID_MEMBER_NAME);
        }

        if (getContacts() == null||getContacts().size()==0) {
            throw new MemberException(CONTACT_IS_EMPTY);
        }

        getContacts().forEach(contact -> {contact.validate();});
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return getId().equals(member.getId()) && getMemberName().equals(member.getMemberName()) && getContacts().equals(member.getContacts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getMemberName(), getContacts());
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", memberName='" + memberName + '\'' +
                ", contacts=" + contacts +
                '}';
    }
}
