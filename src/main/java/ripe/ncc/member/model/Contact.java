package ripe.ncc.member.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import ripe.ncc.member.Constants;
import ripe.ncc.member.exception.MemberException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import static ripe.ncc.member.util.StringValidatorUtil.validateNullFalse;
import static ripe.ncc.member.util.StringValidatorUtil.validateNullTrue;

@Entity
public class Contact implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    @JsonProperty("name")
    private String contactName;

    @Column(nullable = false, unique = true)
    private String email;

    private String phone;

    protected static final String INVALID_CONTACT_NAME = "Invalid contact name";
    protected static final String INVALID_EMAIL_ADDRESS = "Invalid email address";
    protected static final String INVALID_PHONE_NUMBER = "Invalid phone number";

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * This method throws a MemberException if contactName or email or phone is not valid
     */
    public void validate() throws MemberException {
        if (!validateNullFalse(getContactName(), Constants.NAME_VALIDATOR)) {
            throw new MemberException(INVALID_CONTACT_NAME);
        }

        if (!validateNullFalse(getEmail(), Constants.EMAIL_VALIDATOR)) {
            throw new MemberException(INVALID_EMAIL_ADDRESS);
        }

        if (!validateNullTrue(getPhone(), Constants.PHONE_VALIDATOR)) {
            throw new MemberException(INVALID_PHONE_NUMBER);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contact)) return false;
        Contact contact = (Contact) o;
        return getContactName().equals(contact.getContactName()) && getEmail().equals(contact.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getContactName(), getEmail());
    }

    @Override
    public String toString() {
        return "Contact{" +
                "contactName='" + contactName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
