package ripe.ncc.member.util;

public class StringValidatorUtil {

    public static boolean validateNullFalse(String mainString, String regex) {
        return mainString == null ? false : mainString.matches(regex);
    }

    public static boolean validateNullTrue(String mainString, String regex) {
        return mainString == null ? true : mainString.matches(regex);
    }
}
