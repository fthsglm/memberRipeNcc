package ripe.ncc.member.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ripe.ncc.member.exception.MemberException;
import ripe.ncc.member.model.Member;
import ripe.ncc.member.repository.MemberRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;

    protected final static String MEMBER_NOT_FOUND_ERR_KEY = "Member not found by id = ";
    protected final static String MEMBER_NOT_NULL = "Null member value";
    protected final static String EMAIL_ALREADY_EXIST = "Email already exist";

    public Member addMember(Member member) throws MemberException {
        if (member == null) {
            throw new MemberException(MEMBER_NOT_NULL);
        }
        member.validate();
        try {
            return memberRepository.save(member);
        } catch (DataIntegrityViolationException cve) {
            throw new MemberException(EMAIL_ALREADY_EXIST);
        }
    }

    public List<Member> findAll() {
        return memberRepository.findAll();
    }

    public Member findById(Long id) throws MemberException {
        Optional<Member> memberOptional = memberRepository.findMemberById(id);
        if (memberOptional.isPresent()) {
            return memberOptional.get();
        }

        throw new MemberException(MEMBER_NOT_FOUND_ERR_KEY + id);
    }


}
