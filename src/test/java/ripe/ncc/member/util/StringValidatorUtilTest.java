package ripe.ncc.member.util;

import org.junit.jupiter.api.Test;
import ripe.ncc.member.Constants;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ripe.ncc.member.util.StringValidatorUtil.validateNullFalse;
import static ripe.ncc.member.util.StringValidatorUtil.validateNullTrue;

class StringValidatorUtilTest {

    @Test
    public void validateNullFalseReturnTrue() {
        assertTrue(validateNullFalse("Fatih", Constants.NAME_VALIDATOR));
    }

    @Test
    public void validateNullFalseReturnFalse() {
        assertFalse(validateNullFalse(null, Constants.NAME_VALIDATOR));
    }

    @Test
    public void validateNullTrueReturnTrue() {
        assertTrue(validateNullTrue(null, Constants.NAME_VALIDATOR));
    }

    @Test
    public void validateNullTrueReturnFalse() {
        assertFalse(validateNullTrue("Fatih3", Constants.NAME_VALIDATOR));
    }
}