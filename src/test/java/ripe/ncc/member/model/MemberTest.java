package ripe.ncc.member.model;

import org.junit.jupiter.api.Test;
import ripe.ncc.member.exception.MemberException;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ripe.ncc.member.model.Member.CONTACT_IS_EMPTY;
import static ripe.ncc.member.model.Member.INVALID_MEMBER_NAME;

public class MemberTest {

    @Test
    public void shouldValidate() {
        Member member = new Member();
        member.setMemberName("MemberName");

        Contact contact = new Contact();
        contact.setContactName("ContactName");
        contact.setPhone("5535424202");
        contact.setEmail("fthsglm88@gmail.com");

        Collection<Contact> contactList=new LinkedList<>();
        contactList.add(contact);
        member.setContacts(contactList);

        member.validate();
    }

    @Test
    public void shouldThrowExceptionInvalidMemberName() {
        Member member = new Member();
        member.setMemberName("Member3Name");

        Contact contact = new Contact();
        contact.setContactName("ContactName");
        contact.setPhone("5535424202");
        contact.setEmail("fthsglm88@gmail.com");

        Collection<Contact> contactList=new LinkedList<>();
        contactList.add(contact);
        member.setContacts(contactList);

        failConditionCall(member, INVALID_MEMBER_NAME);
    }

    @Test
    public void shouldThrowExceptionWhenContactIsNull() {
        Member member = new Member();
        member.setMemberName("MemberName");

        failConditionCall(member, CONTACT_IS_EMPTY);
    }

    @Test
    public void shouldThrowExceptionWhenContactIsEmpty() {
        Member member = new Member();
        member.setId(1L);
        member.setMemberName("MemberName");

        member.setContacts(Collections.EMPTY_LIST);

        failConditionCall(member, CONTACT_IS_EMPTY);
    }

    private void failConditionCall(Member member, String expectedFailError) {
        try {
            member.validate();
        } catch (MemberException me) {
            assertEquals(expectedFailError, me.getMessage());
        }
    }
}