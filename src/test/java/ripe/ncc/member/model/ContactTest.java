package ripe.ncc.member.model;

import org.junit.jupiter.api.Test;
import ripe.ncc.member.exception.MemberException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ripe.ncc.member.model.Contact.*;

public class ContactTest {

    @Test
    void shouldValidate() {

        Contact contact = new Contact();
        contact.setContactName("ContactName");
        contact.setPhone("5535424202");
        contact.setEmail("fthsglm88@gmai.com");

        contact.validate();
    }

    @Test
    void shouldThrowExceptionNotValidNameValidate() {

        Contact contact = new Contact();
        contact.setContactName("Contac3tName");
        contact.setPhone("905535424202");
        contact.setEmail("fthsglm88@gmai.com");

        failConditionCall(contact, INVALID_CONTACT_NAME);
    }

    @Test
    void shouldThrowExceptionNotValidEmailValidate() {

        Contact contact = new Contact();
        contact.setContactName("ContactName");
        contact.setPhone("905535424202");
        contact.setEmail("fthsglm88gmai.com");

        failConditionCall(contact, INVALID_EMAIL_ADDRESS);


    }

    @Test
    void shouldThrowExceptionNotValidPhoneValidate() {

        Contact contact = new Contact();
        contact.setContactName("ContactName");
        contact.setPhone("905A35424202");
        contact.setEmail("fthsglm88@gmai.com");

        failConditionCall(contact, INVALID_PHONE_NUMBER);
    }

    private void failConditionCall(Contact contact, String expectedFailError) {
        try {
            contact.validate();
        } catch (MemberException me) {
            assertEquals(expectedFailError, me.getMessage());
        }
    }
}