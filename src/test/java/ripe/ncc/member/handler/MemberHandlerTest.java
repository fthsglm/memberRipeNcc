package ripe.ncc.member.handler;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ripe.ncc.member.exception.MemberException;
import ripe.ncc.member.model.Contact;
import ripe.ncc.member.model.Member;
import ripe.ncc.member.service.MemberService;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class MemberHandlerTest {

    @InjectMocks
    private MemberHandler memberHandler;

    @Mock
    private MemberService memberService;

    @Test
    public void shouldGetAllMembers() throws Exception {
        Long memberId = 1L;
        Member member = new Member();
        member.setId(memberId);
        List<Member> memberList = new LinkedList<>();

        when(memberService.findAll()).thenReturn(memberList);

        ResponseEntity<List<Member>> expectedResponse = new ResponseEntity<>(memberList, HttpStatus.OK);

        ResponseEntity<List<Member>> response = memberHandler.getAllMembers();

        verify(memberService).findAll();
        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnOkGetMemberById() throws Exception {
        Long memberId = 1L;
        String memberName = "member Name";
        Member member = new Member();
        member.setId(memberId);
        member.setMemberName(memberName);
        when(memberService.findById(memberId)).thenReturn(member);
        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(member, HttpStatus.OK);

        ResponseEntity<Member> response = memberHandler.getMemberById(memberId);

        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnNotFoundGetMemberById() throws Exception {
        when(memberService.findById(anyLong())).thenThrow(new MemberException(""));
        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        ResponseEntity<Member> response = memberHandler.getMemberById(1L);

        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnOkAddMember() throws Exception {

        Long memberId = 1L;
        Member member = new Member();
        member.setId(memberId);

        when(memberService.addMember(member)).thenReturn(member);

        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(member, HttpStatus.CREATED);

        ResponseEntity<Member> response = memberHandler.addMember(member);

        verify(memberService).addMember(member);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnBadRequestAddMemberWhenContactListNull() throws Exception {

        Long memberId = 1L;
        Member member = new Member();
        member.setId(memberId);
        when(memberService.addMember(any(Member.class))).thenThrow(new MemberException(""));
        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        ResponseEntity<Member> response = memberHandler.addMember(member);

        verify(memberService).addMember(member);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnBadRequestAddMemberWhenContactListEmpty() throws Exception {

        Long memberId = 1L;
        Member member = new Member();
        member.setId(memberId);
        when(memberService.addMember(any(Member.class))).thenThrow(new MemberException(""));
        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        ResponseEntity<Member> response = memberHandler.addMember(member);

        verify(memberService).addMember(member);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void shouldReturnBadRequestAddMemberWhenDataIntegrityException() throws Exception {

        Long memberId = 1L;
        Member member = new Member();
        member.setId(memberId);
        when(memberService.addMember(any(Member.class))).thenThrow(new DataIntegrityViolationException(""));
        ResponseEntity<Member> expectedResponse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        ResponseEntity<Member> response = memberHandler.addMember(member);

        verify(memberService).addMember(member);
        assertEquals(expectedResponse, response);
    }
}