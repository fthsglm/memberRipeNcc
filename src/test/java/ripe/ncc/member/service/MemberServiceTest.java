package ripe.ncc.member.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import ripe.ncc.member.exception.MemberException;
import ripe.ncc.member.model.Contact;
import ripe.ncc.member.model.Member;
import ripe.ncc.member.repository.MemberRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static org.hamcrest.Matchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ripe.ncc.member.service.MemberService.*;

@SpringBootTest
class MemberServiceTest {

    @InjectMocks
    private MemberService memberService;

    @Mock
    private MemberRepository memberRepository;

    @Test
    void shouldAddMember() {

        Member member = new Member();
        member.setId(1L);
        member.setMemberName("Fatih");

        Contact contact = new Contact();
        contact.setContactName("RipeNCC");
        contact.setEmail("fthsglm88@gmail.com");
        contact.setPhone("5535424202");

        Collection<Contact> contactList=new LinkedList<>();
        contactList.add(contact);
        member.setContacts(contactList);
        when(memberRepository.save(member)).thenReturn(member);

        Member responseMember=memberService.addMember(member);

        verify(memberRepository).save(member);
        assertEquals(member,responseMember);
    }

    @Test
    void shouldThrowExceptionWhenMemberNull() {
        Member member = null;

        try {
            memberService.addMember(member);
        } catch (MemberException me) {
            assertEquals(MEMBER_NOT_NULL, me.getMessage());
        }
    }

    @Test
    void shouldThrowExceptionEMailAlreadyExist() {
        Member member = new Member();
        member.setMemberName("Fatih");

        Contact contact = new Contact();
        contact.setContactName("RipeNCC");
        contact.setEmail("fthsglm88@gmail.com");
        contact.setPhone("5535424202");

        Collection<Contact> contactList=new LinkedList<>();
        contactList.add(contact);
        member.setContacts(contactList);
        when(memberRepository.save(member)).thenThrow(new DataIntegrityViolationException(""));

        try {
            memberService.addMember(member);
        } catch (MemberException me) {
            assertEquals(EMAIL_ALREADY_EXIST, me.getMessage());
        }
    }

    @Test
    void shouldFindById() {
        Long id = 1L;
        Member member = new Member();
        member.setId(id);
        Optional<Member> optMember = Optional.of(member);
        when(memberRepository.findMemberById(id)).thenReturn(optMember);

        Member resultMember = memberService.findById(id);

        assertEquals(member, resultMember);
    }

    @Test
    void shouldThrowExceptionFindById() {
        Long id = 1L;
        when(memberRepository.findMemberById(id)).thenReturn(Optional.ofNullable(null));
        try {
            memberService.findById(id);
        } catch (MemberException me) {
            assertTrue(me.getMessage().startsWith(MEMBER_NOT_FOUND_ERR_KEY));
        }
    }

    @Test
    void shouldFindAll() {
        memberService.findAll();
        verify(memberRepository).findAll();
    }
}