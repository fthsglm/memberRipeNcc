FROM openjdk:11
WORKDIR /app
COPY . .
RUN chmod +x mvnw && ./mvnw clean install -U -DskipTests
CMD ["./mvnw","spring-boot:run","-Dspring-boot.run.profiles=docker"]